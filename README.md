# README #

A plugin to try to make it a little easier to make a standalone Asciidoctor presentation. The plugin automatically clones the deck.js infrastructure for you, so you don't have to fuss with finding the right projects and installing them as submodules. It adds less support and implies a standard template build file that lets you insert your content and go.

[original](https://github.com/tlberglund/asciidoctor-presentation)