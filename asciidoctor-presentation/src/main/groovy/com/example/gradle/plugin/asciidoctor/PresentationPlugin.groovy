package com.example.gradle.plugin.asciidoctor

import org.gradle.api.Plugin
import org.gradle.api.Project


class PresentationPlugin implements Plugin<Project> {

    @Override
    void apply(Project project) {
        project.plugins.apply('lesscss')
        project.plugins.apply('org.asciidoctor.gradle.asciidoctor')
        project.tasks.create('slides', PresentationTask)
        project.tasks.create('presentation').configure {
            dependsOn << ['lessc', 'slides']
        }
        project.tasks.create('framework').configure {
            dependsOn << ['deckjs', 'backend']
        }

        project.tasks.create('deckjs', FrameworkTask).configure {
            frameworkDir = 'deck.js'
            uri = 'https://github.com/imakewebthings/deck.js.git'
        }

        project.tasks.create('backend', FrameworkTask).configure {
            frameworkDir = 'asciidoctor-deck.js'
            uri = 'https://github.com/asciidoctor/asciidoctor-deck.js.git'
        }
        project.tasks.getByName('lessc').configure {
            sourceDir "src/less"
            include "**/*.less"
            destinationDir = "${project.buildDir}/asciidoc/deckjs/deck.js/themes/style"
            mustRunAfter project.tasks.getByName('slides')
            doLast {
                project.copy {
                    from('src/less') {
                        include '**/*.css'
                    }
                    into destinationDir
                }
            }
        }
        def modules = project.container(Module) { name ->
            new Module(name)
        }
        project.configure(project) {
            extensions.create('presentation', ModuleExtension, modules)
        }

    }
}
